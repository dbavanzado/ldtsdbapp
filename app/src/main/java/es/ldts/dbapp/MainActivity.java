package es.ldts.dbapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

	private static Context context;

	private TextView pizarra;
	private Button refresh;

	private CharSequence mTitle;

	private android.support.v7.widget.ShareActionProvider mShareActionProvider = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = getBaseContext();
		//El Contexto es el entorno de la aplicación: Carpetas, idioma, etc.
		setContentView(R.layout.activity_main);

		//INICIALIZO LOS ELEMENTOS GRÁFICOS DE LA PANTALLA (layout) DE ESTA ACTIVIDAD
        pizarra = (TextView) findViewById(R.id.pizarra);
        refresh = (Button) findViewById(R.id.refresh);

		//ActionBar es la barra superior de la aplicación
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		//Configuramos icono, título y menu
		getSupportActionBar().setIcon(R.mipmap.ic_launcher);
		setTitle(context.getResources().getString(R.string.app_name));
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		wrpizzara();

	}

	public void wrpizzara() {

		//1-. EJEMPLO DE STRING
		String texto = "Esto es una variable de texto sin más...\n";

		//2-. EJEMPLO CON MATES
		texto += "Voy a añadir una línea con un número aleatorio: ";
		final int min = 20;
		final int max = 80;
		final int random = new Random().nextInt((max - min) + 1) + min;
		texto += "" + random + "\n";

		//3-. EJEMPLO CON COLORES Y PROPIEDADES DE ELEMENTOS GRÁFICOS
		texto += "O poner un color aleatorio... ";
		Random rnd = new Random();
		int color = Color.argb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
		texto += "" + color + "\n";

		//4-. EJEMPLO CON FECHA Y HORA
		texto += "Y si quieres la hora: Son las ";
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		texto += "" + hour + ":" + minute + ":" + second;

		pizarra.setText(texto);
		pizarra.setBackgroundColor(color);
	}

	//Este método se crea para que el botón de Refresh pueda ejecutar wrpizarra.
	// Los botones siempre ejecutan métodos que recibien como parámetro el View actual.
	public void wrpizzara(View v) {
		wrpizzara();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.share_menu, menu);
		MenuItem itemShare = menu.findItem(R.id.menu_item_share);
		itemShare.setIcon(R.mipmap.share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(itemShare);
		Intent j = new Intent(Intent.ACTION_SEND);
		setShareIntent(j);
		doShare();
		// Return true to display menu
		return true;
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return true;
	}


	// SHARE BUTTON!!!!
	// Call to update the share intent
	//@Override
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	public void doShare() {
		// populate the share intent with data
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.sharetxt) +
				"\n" + context.getResources().getString(R.string.app_name) + "\n" +
				context.getResources().getString(R.string.sharelink)
		);
		mShareActionProvider.setShareIntent(intent);
	}

	private void exit() {
		new AlertDialog.Builder(this)
				.setTitle(R.string.app_name)
				.setMessage(context.getResources().getStringArray(R.array.exitbuttom)[0])
				.setPositiveButton(context.getResources().getStringArray(R.array.exitbuttom)[1], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//finish();
						//finishActivity(0);
						System.exit(0);
					}
				})
				.setNegativeButton(context.getResources().getStringArray(R.array.exitbuttom)[2], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				})
				.show();
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	public void onBackPressed() {
		exit();
		//your code when back button pressed
	}


}
